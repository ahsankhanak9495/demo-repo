<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<meta name="csrf-token" content="{{ csrf_token() }}" />

    <div class="form-group row">
        <label for="first_name" class="col-xs-12 col-form-label">Product Name</label>
        <div class="col-xs-10">
            <input class="form-control" type="text" value="{{ old('p_name') }}" name="p_name" required id="p_name" placeholder="Product Name">
        </div>
    </div>

    <div class="form-group row">
        <label for="last_name" class="col-xs-12 col-form-label">quantity </label>
        <div class="col-xs-10">
            <input class="form-control" type="number" value="{{ old('quantity') }}" name="quantity" required id="quantity" placeholder="quantity">
        </div>
    </div>

    <div class="form-group row">
        <label for="email" class="col-xs-12 col-form-label">price</label>
        <div class="col-xs-10">
            <input class="form-control" type="number" required name="price" value="{{old('price')}}" id="price" placeholder="price">
        </div>
    </div>



    <div class="form-group row">
        <label for="zipcode" class="col-xs-12 col-form-label"></label>
        <div class="col-xs-10">
            <button onclick="addprod()" class="btn btn-primary">Add</button>
        </div>
    </div>

<div id="table">
    <table class="table">
        <tr id="taxonomy-table">
            <th>Product name</th>
            <th>Price</th>
            <th>Quantity</th>
        </tr>

    </table>
</div>
<script>
    function addprod() {
        var prduct = $('#p_name').val();
        var quantity = $('#quantity').val();
        var price = $('#price').val();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        if (prduct != '' && quantity !== '' && price != '' ){
            $.ajax({
                url: "/prod-add",
                type: "POST", /* or type:"GET" or type:"PUT" */
                dataType: "json",
                data: {_token: CSRF_TOKEN,name:prduct,quantity:quantity,price:price},
                success: function (result) {
                    var size = 0, key;
                    for (key in result) {
                        if (result.hasOwnProperty(key)) size++;
                    }

                    for (var i = 0; i < size; i++){
                        $("#table").after('<tr class="row-table-'+i+'">' +
                            '<td>'+result.name+'</td>' +
                            '<td>'+result.price+'</td>' +
                            '<td>'+result.quantity+'</td>' +
                            '</tr>');
                    }

                },
                error: function () {
                    alert("invalid fields");
                }
            })
        }
    }
</script>